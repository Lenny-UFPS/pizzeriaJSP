![Pizzería](http://www.madarme.co/portada-web.png)
# Título del proyecto:

#### Web APP - Inserción de Datos a una base de datos remota a través de un archivo JSON
***
## Índice
1. [Características](#caracteristicas)
2. [Tecnologías](#tecnologías)
3. [IDE](#ide)
4. [Instalación](#instalación)
5. [Demo](#demo)
6. [Autor(es)](#autores)
7. [Institución Académica](#institución-académica)
***

#### Características

  - Proyecto con lectura de datos JSON a través de Gson
  - Carga dinámica del JSON
  - Archivo JSON de ejemplo: [ver](https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json)

***
#### Tecnologías

  - HTML5
  - Java - JSP
  - [TailwindCSS](https://tailwindcss.com/), Framework utilizado para el desarrollo de las vistas (Front - End)

  ***
#### IDE

- El proyecto se desarrolla usando Netbeans 8.2 RC. [Descargar](https://netbeans.org/downloads/8.2/rc/) 
- JSON Viewer - (http://jsonviewer.stack.hu/)

***
### Instalación

Firefox Devoloper Edition - [Descargar](https://www.mozilla.org/es-ES/firefox/developer/).


```sh
-Descargar proyecto
-Invocar página index.html desde Mozilla Firefox 
```

***
### Demo

Para ver el demo de la aplicación puede dirigirse a: [COVID-19 | Inicio](http://ufps33.madarme.co/Pizzeria).

***
### Autor(es)
Proyecto desarrollado por:
- Javier Eduardo Contreras Castro (<javiereduardocc@ufps.edu.co>).

***
### Institución Académica   
Proyecto desarrollado en la Materia Programación Web - Grupo B del [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
   
